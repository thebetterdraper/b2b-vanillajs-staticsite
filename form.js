jq(document).ready(

    function() {
        var form_flag = 0;


        var url_address = "https://okh2i2itra.execute-api.ap-south-1.amazonaws.com/dev/api/insert/";
        var slack_endpoint="https://hooks.slack.com/services/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";

        jq(".contact_dets").click(function() {

            jq(".contact_dets").css("border", "2px solid #39C9BB");
            jq("#contact_content").css("border", "2px solid #39C9BB");
        });
        jq("#contact_content").click(function() {
            jq("#contact_content").css("border", "2px solid #39C9BB");
        });
        jq("#contact_email").click(function() {
            jq("#contact_email").css("border", "2px solid #39C9BB");
        });


        function padDigits(num) {
        
            var num_padded=num;
            if (num.toString().length != 2)
                num_padded = "0" + num;

            return num_padded;
        }

        function getTimestamp() {
            var currentdate = new Date();
            var datetime = currentdate.getFullYear() + "-" + padDigits(currentdate.getMonth()+1) + "-" + padDigits(currentdate.getDate());

            var yeardets = padDigits(currentdate.getHours()) + ":" +
                padDigits(currentdate.getMinutes()) + ":" +
                padDigits(currentdate.getSeconds());

            return datetime + " " + yeardets;
        }

        jq("#submit").click(function() {

            form_flag = 0;
            console.log("Clicked");
            jq(".contact_dets").each(function(i, elem) {
                if (jq(this).val() === "") {
                    jq(this).css("border", "2px solid red");
                    form_flag++;
                }
            });

            jq(".contact_content_class").each(function(i, elem) {
                if (jq(this).val() === "") {
                    jq(this).css("border", "2px solid red");
                    form_flag++;
                }
            });





            if (form_flag == 0) {
                console.log("HEnters");

                var data_send = {
                    timestamp: getTimestamp(),
                    type: "B2B",
                    name: jq("#contact_name").val(),
                    email: jq("#contact_email").val(),
                    number: jq("#contact_phone").val(),
                    feedback: jq("#contact_content").val()
                }
                
                var data_send_as_text=JSON.stringify(data_send);
                var text_val={
                    text:data_send_as_text
                }

                console.log(data_send);

                jq.ajax ({
                    url: slack_endpoint,
                    type: "POST",
                    data:JSON.stringify(text_val),
                    success: function(){
                            console.log("Sent");
                    }
                });
                
                jq.post(url_address, data_send, function(data, status) {
                    onSuccess(data, status);
                });
                
                

                // onSuccess("ooo", "Success");





            }




            function onSuccess(d, s) {
                console.log(d);
                console.log(s);
                if (d.status === "Success") {

                    jq("#contact_form_heading").html("Thank You.<p> Your response has been submitted.</p>");
                    jq(".contact_dets").each(function(i, elem) {
                        jq(this).css("display", "none");

                    });
                    jq("#contact_form_heading").css("padding-top", "60px");
                    jq("#contact_form_heading").css("font-size", "24px");
                    jq(".contact_content_class").each(function(i, elem) {
                        jq(this).css("display", "none");

                    });

                    jq("#submit").css("display", "none");


                } else if (d.status === "Invalid Email ID") {

                    window.alert("Email ID is invalid. Please enter a correct one.");
                    jq("#contact_email").css({
                        "border": "2px solid red"
                    })

                }
                else{
                 window.alert("Something went wrong. Please try again.");
                }
                
            }






        });
    });